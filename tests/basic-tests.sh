set -e

# TODO Change for the proper zone once we have it
ZONE=webtest.dns-manager.cern.ch

echo Run the job of the Oracle Table Synchronizer and check if 10 resources were created
oc create job --from=cronjob.batch/oracle-table-synchronizer test
JOB_STATUS=$(oc get job test -o json | jq -r .status.conditions[0].type)
while [ $JOB_STATUS = "null" ]; do
    JOB_STATUS=$(oc get job test -o json | jq -r .status.conditions[0].type)
    if [ $JOB_STATUS = "Complete" ]; then
        break
    elif [ $JOB_STATUS = "Failed" ]; then
        echo 'Oracle Table Synchronizer job ran and failed' >&2
        exit 1
    fi
done
test "10" == "$(oc get dnsendpoints.internal.${ZONE} -o name | wc -l)"
echo -e "OK\n"

