{{/* vim: set filetype=mustache: */}}

{{/*
Return the proper External DNS image name
*/}}
{{- define "external-dns.image" -}}
{{- $registryName := .Values.image.registry -}}
{{- $repositoryName := .Values.image.repository -}}
{{- $tag := .Values.image.tag | toString -}}
{{- printf "%s/%s:%s" $registryName $repositoryName $tag -}}
{{- end -}}

{{- define "external-dns.app-name" -}}
{{- printf "%s-%s" .Values.name .Values.viewName | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "external-dns.owner-id" -}}
{{- printf "%s-%s-%s" .Values.environmentName .Values.name .Values.viewName | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
----- CRD definitions -----
*/}}

{{- define "external-dns.groapiversion" -}}
{{- printf "%s/%s" .Values.crd.group .Values.crd.apiVersion | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "external-dns.crd-name" -}}
{{- printf "%s" "dnsendpoint" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "external-dns.crd-plural" -}}
{{- $name := (include "external-dns.crd-name" . | trunc 63 | trimSuffix "-") -}}
{{- printf "%s%s" $name "s" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "external-dns.crd-status" -}}
{{- $name := (include "external-dns.crd-plural" . | trunc 63 | trimSuffix "-") -}}
{{- printf "%s/%s" $name "status" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
----- END of crd definitions -----
*/}}
