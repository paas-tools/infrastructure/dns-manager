# DNS manager deployment

This project deploys various components to manage the integration of Openshift applications with
CERN DNS environment.

As per [KB0006299](https://cern.service-now.com/service-portal/article.do?n=KB0006299) (see also
dev notes in [CIPAAS-379](https://its.cern.ch/jira/browse/CIPAAS-379)), Openshift applications
can use a variety of DNS names. They include:

* a number of "standard" DNS zones owned and managed by CERN WebServices
 (`*.web.cern.ch`, `*.app.cern.ch`...). These are "delegated domains" (see below)
 from the CERN network team's point of view.
* "non-standard" names in the `.cern` and `.cern.ch` top-level domains
  or custom domains entirely, which WebServices does not own and need to be managed separately.

The DNS manager consists of the following components:
* [external-dns](./chart/charts/external-dns) deploys the external-dns upstream component, responsible for dynamically
  managing the WebServices-owned DNS zones using a set of Custom Resources (CRs) as source of truth.
  See [CIPAAS-245](https://its.cern.ch/jira/browse/CIPAAS-245) and [WFP-2](https://its.cern.ch/jira/browse/WFP-2).
* [openshift-acme](./chart/charts/openshift-acme) provides certificates from [Let's Encrypt](https://letsencrypt.org)
  for applications that do not use a DNS name in one of the supported DNS zones (this is not necessary for applications
  using the standard zones since the default Openshift router certificate
  has SAN entries for each DNS zone: `*.web.cern.ch`, `*.app.cern.ch`)
* [oracle-table-sync](./chart/charts/oracle-table-sync) deploys the program [Oracle table synchronizer](https://gitlab.cern.ch/paas-tools/networking/oracle-table-synchronizer) 
  which reads entries from an Oracle view (ZONE_WEB) and then creates CRs used by external-dns to update the DNS tables. 
  As of September 2019, we are using this program to ease the transition from the old design we managed the DNS to the new design (see notes below).

## CERN DNS infrastructure

The CERN DNS is comprised of two views, this allows us to manage independent DNS zones,
for ```internal``` (content of the zone served by the DNS servers used by machines
in the CERN network) and ```external```  (content of the zone served by the DNS
servers used by Internet clients). However, for the time being, this feature is
not interesting to us.
It's more of a challenge since we have to duplicate every record to maintain
consistency of the DNS names inside and outside the CERN network.

### Delegated domains

The DNS zones managed by this component are "delegated domains" provided by
[the DNS service](http://service-dns.web.cern.ch/service-dns/).

They consist of subdomains of `cern.ch`: `web.cern.ch`, `docs.cern.ch` etc.
New zones can be requested from the [Network Operations FE](https://cern.service-now.com/service-portal/function.do?name=Network-Operations).

They are managed via [DynDNS (RFC2136)](http://service-dns.web.cern.ch/service-dns/rfc2136.txt).
DynDNS uses [TSIG](https://en.wikipedia.org/wiki/TSIG) signatures for authentication and authorization.

The TSIG keys are provided by the DNS service. As of September 2019 we use common
keys for all subdomains we manage, but there is a separate key for the `internal`
and `external` views (see below).

## External-dns deployment overview

The DNS manager is deployed using a Helm chart that deploys an instance of
[External-DNS](https://github.com/kubernetes-incubator/external-dns)
for each view of each DNS zone declared in the ```domains``` values in the file ```chart/charts/external-dns/values.yaml```.

We also deploy a CRD of `kind: DNSEndpoint`, each instance of External-DNS is going to
manage a subset of this CRD filtering them by a set of annotations:
```yaml
dns-manager.webservices.cern.ch/domain: <zone> # e.g webtest.cern.ch
dns-manager.webservices.cern.ch/view: <view> # e.g internal
```
we have to do this due to a limitation of 
[External-DNS](https://github.com/kubernetes-incubator/external-dns) (see limitations below).

Note that we also copy these annotations to the labels to ease CLI operations.

### Note about the external-dns Helm chart organization

The external-dns subchart has a particularity in comparison to the majority of other helm charts.
When it is being deployed it will go through every view of every zone in the values of the field
```domains``` in the file ```chart/charts/external-dns/values.yaml``` and deploy all the templates whose filename starts with
```_``` for each view. This logic is described in the file
[chart/charts/external-dns/templates/externaldns-instances.yaml](chart/charts/external-dns/templates/externaldns-instances.yaml).

When the external-dns subchart is deployed it also installs all the CRDs inside the folder crds.

### Important external-dns chart values

There are a set of important values that can have a very high impact on the deployment of the system, we will now go over each one.

#### Environment Name

Each record added by [External-DNS](https://github.com/kubernetes-incubator/external-dns)
to the DNS gets created with an associated `TXT` record with ownership information.
This allows [External-DNS](https://github.com/kubernetes-incubator/external-dns)
to know what records it manages and prevents it from messing up previously created
DNS entries or DNS entries managed by another instance of External-DNS.

Part of the information in this `TXT` record is a deployment parameter named ```txt-owner-id```.
Since we will have multiple deployments in the different clusters/environments we need this value
to be unique in the different deployments to prevent two instances of external DNS
in different clusters/environments from interfering with each other.
In our deployment configuration, this value is named ```environmentName```.

#### Dry Run

Another important value to mention is the flag ```dryRun```, this flag allows us to run an instance of External-DNS without affecting the DNS registry. 

### Multiple deployments

As of January 2020, we intend to deploy all the different zones in each cluster, this will allow us to manage a subset of records of each zone in any cluster, each instance will not interfere with the DNS records of the others since we have the variable `environmentName`, how this works explained [here](#environment-name).

## Openshift-acme deployment overview

[Let's Encrypt](https://letsencrypt.org) requires applications to be accessible from Internet to validate DNS domain ownership.
This openshift-acme is only functional in the prod Openshift environment as it's the only one that can be accessed from Internet.
The prod environment should thus use the Let's Encrypt "live" environment, while other clusters (not reachable from Internet)
are configured with the ["staging" environment](https://letsencrypt.org/docs/staging-environment/) and are not functional.

A Let's Encrypt account is created automatically by openshift-acme.

Because we need Acme validation routes to be Internet-visible so Let's Encrypt servers can validate domain ownership,
and routes are only visible to Intranet by default, we need
[a specific OPA rule](https://gitlab.cern.ch/paas-tools/infrastructure/openpolicyagent/merge_requests/6)
that override the default behavior and makes openshift-acme's validation routes visible to Internet.

We use the full SH256 to try to protect ourselves from any possible hijacking of the upstream
images hostes on quay.io.

### Updating controller and exposer images

For this please refer to [okd4-deployment/dns-manager/README.md](https://gitlab.cern.ch/paas-tools/okd4-deployment/dns-manager#updating-controller-and-exposer-images)
## Oracle table synchronizer overview
[Oracle table synchronizer](https://gitlab.cern.ch/paas-tools/networking/oracle-table-synchronizer) a program that reads 
entries from an Oracle view (ZONE_WEB) and then creates CRs of certain ```kind``` and ```group``` depending on the inputed parameters. 
The purpose of this deployment is to ease the transition from the old design to the new one (see notes below) by creating sets of 
CRs that contain the information present in the Oracle database.

### Updating the database connection string

 [Oracle table synchronizer](https://gitlab.cern.ch/paas-tools/networking/oracle-table-synchronizer) connects himself file to connect 
 to the Oracle database ```cerndb1```. For this, it uses an connection string present in the file ```chart/charts/oracle-table-sync/values.yaml``` this
  connection string might need to be updated in case the database team updates it. 
 
 To update check the current connection string go [here](http://service-oracle-tnsnames.web.cern.ch/service-oracle-tnsnames/tnsnames.ora) search 
 for ```cerndb1```. The connection string is a concatenation of fields ```HOST:PORT/SERVICE_NAME``` so just copy these values in the fashion described 
 to the field ```database``` in the file ```chart/charts/oracle-table-sync/values.yaml``` and it should work.

# Deploying to a new environment/cluster

## Prerequisites

Some preparation steps need to be performed with cluster-admin permissions. This needs to be done only once per Openshift cluster/environment.

Start a Docker container for installation (mounting this git repo in /project):

```bash
docker run --rm -it -v $(pwd):/project -e TILLER_NAMESPACE=paas-infra-dnsmanager gitlab-registry.cern.ch/paas-tools/openshift-client:v3.11.0 bash
```

Then in that container:

```bash
# login as cluster admin to the destination cluster
oc login https://openshift-test.cern.ch #or openshift-dev.cern.ch or openshift.cern.ch
export HELM_VERSION=3.0.2 # make sure to use the same Helm version found in .gitlab-ci.yml
curl "https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar zx
mv --force linux-amd64/helm /usr/bin/
# create namespace and serviceaccounts for deployment
helm template /project/prerequisites/ | oc create -f -
# obtain the token for the Tiller service account
oc serviceaccounts get-token -n paas-infra-dnsmanager tiller
```

The rest of the deployment will be handled by the common GitLab CI in [infrastructure-ci](https://gitlab.cern.ch/paas-tools/infrastructure-ci/blob/master/helm-ci-templates/Base.gitlab-ci.yml).

Set the per-environment `TILLER_TOKEN` and `HELM_SECRETS` CI/CD variables as per [README of the infrastructure-ci project](https://gitlab.cern.ch/paas-tools/infrastructure-ci/tree/master#prerequisites).
For this project, the content of the `HELM_SECRETS` mentioned in the README should at least have the following content:

- keys to manage each view, the environment where we want to deploy to as well as the username and password of the account with read privileges to the view ```ZONE_WEB``` in the Oracle database ```cerndb1```:

```yaml
external-dns:
  environmentName: prod
  views:
    external:
      key: xxxxxxxxxxx
    internal:
      key: yyyyyyyyyyy
oracle-table-synchronizer:
  oracle:
    username: webreg_r
    password: zzzzzzzzzzzz
```

## Deploy the DNS manager

Once the GitLab CI variables have been populated, use the GitLab CI pipelines to deploy the DNS manager components.

Running the pipeline again will update the deployment as necessary.

## Uninstall the DNS manager components

Start a Docker container for installation (mounting this git repo in /project):

```bash
docker run --rm -it -v $(pwd):/project -e TILLER_NAMESPACE=paas-infra-dnsmanager gitlab-registry.cern.ch/paas-tools/openshift-client:v3.11.0 bash
```

Then in that container:

```bash
# login as cluster admin to the destination cluster
oc login https://openshift-test.cern.ch #or openshift-dev.cern.ch or openshift.cern.ch
# install tiller
export HELM_VERSION=3.0.2
curl "https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar zx
mv --force linux-amd64/helm /usr/bin/
# delete dnsmanager deployment, then prerequisites
helm uninstall dnsmanager
oc delete crd -l created-by-helm-crd-install-for-release=dnsmanager
helm template /project/prerequisites | oc delete -f -
```

### Redeploying dns-manager chart without disrupting the DNS records

If we need to redeploy the dns-manager chart we have to do it without losing any information and without disrupting the DNS. For this, we first need to do some steps for the instances that have records in CRs and are running with `dryRun` set to `false`. Once we identified these instances we have to do an upgrade to the deployment and set their `dryRun` flag to `true` so that they don't tamper with the DNS right after the chart is installed and there is still no CRs. You do this by editing the file `chart/charts/external-dns/values.yaml` and then upgrading the deployment with GitLab CI.

Once we have upgraded these instances we can safely uninstall the chart following the procedure above, **do not** run the last command which deletes the namespace. 

After uninstalling the old chart we can now head over to GitLab and run the CI job that deploys the new chart. When the job finishes we can use the container that we used to uninstall the chart to check if the deployment is looking good. If we don't see any problems we now have to run the Oracle table synchronizer job to re-generate the CRs that we had previously. You can do this by running the following command:

```shell
oc create job --from=cronjob.batch/oracle-table-synchronizer recreate-crs
```

After the job runs and succeeds you can run:

```shell
oc delete job recreate-crs
```

Finally, once this is done we can now ser the flag `dryRun` back to `true` in the instances that had it at `true`. You do this by editing the file `chart/charts/external-dns/values.yaml` and then upgrading the deployment with GitLab CI.

NB: As of January 2020, we can directly delete the chart because all the current CRs that we have are automatically generated by the Oracle table synchronizer, once this changes we will have to come up with a different way of doing this process.

# Zones operations
## Adding a new Zone
To add a new zone we first have to request the Zone to the Networking team, the requested Zone has to be managed via DynDNS (RFC2136), which uses TSIG signatures for authentication and authorization, so the Networking team should also
grant privileges to our TSIG keys (currently itwebservices_external_key and itwebservices_internal_key)to manage the zone (eg. RQF1265526). 
If we are adding an already existing zone we only have to ask the networking team to make it so that that zones starts being managed by DynDNS with again the TSIG keys being ours (eg. RQF1446961).

We then have to add the new zone to the list of domains under `chart/charts/external-dns/values.yaml`. To do this simply paste under `domains` the following pice of yaml text replacing `ZONE` by the zone we are trying to add.
```yaml
ZONE:
  domain: ZONE.cern.ch
  views:
    external:
      enabled: true
      logLevel: info
    internal:
      enabled: true
      logLevel: info
```

Once this is done we can upgrade the helm chart.

NB: if the zone only has one view (eg. internal) you don't need the section `ZONE.view.external`.

## Removing a Zone

To remove a zone first we have to delete that zone from the list of domains under `chart/charts/external-dns/values.yaml` and then deploy the change to each environment. After we will have to remove the CRD that contained the records of the zone we are removing to do this you will have to run manually one of these commands, this happens since CRDs in the crds folder stop being managed by helm after the chart installation:

```shell
oc delete -f chart/charts/external-dns/crd/file-name-of-crd-to-be-removed.yaml
```

or

```shell
oc delete crd name-of-crd-to-be-removed
```

## Design overview

### Old design and motivation for use of external-dns

Legacy webservices uses an Oracle database as the source of truth to populate a single zone `web.cern.ch` in the DNS servers. (Actually there are 3 zones with identical content: `web.cern.ch`, `home.cern.ch`, `webtest.cern.ch` but only web is used in practice)

As of September 2019, with the [ongoing rework of web hosting](https://discourse.web.cern.ch/c/wfp), we want to:
* support more DNS zones than just `web.cern.ch`: `docs.cern.ch`, `app.cern.ch` etc.
* use Openshift as the source of truth for all DNS zones, so we can manage site creation and lifecycle with Kubernetes/Openshift operators

### Challenges and objectives for new design

With this project, we want to retire the use of the Oracle database as well as start managing different DNS zone dynamically.

However, there are some challenges associated with this task:
- New zones should be added easily without requiring much configuration;
- Each zone is at max comprised of two views (external and internal) that have to be managed separately and also might have different access keys;
- Openshift should start being the source of truth for populating the DNS tables;
- We should still be able to set manually certain entries;
- The DNS manager should not modify entries not managed by him, to prevent it from messing up DNS tables;
- Users should not be affected negatively by the new system.

### New design

Given the constraints described above, our new design is based on the idea of having an operator responsible for managing the different zones. This operator will read the desired DNS state from a set of CRs and will update each zone accordingly.

We chose the open-source project [External-DNS](https://github.com/kubernetes-incubator/external-dns) as our DNS manager as it checked almost all the boxes in the sense that can use as source of truth a set of CRs and it supports DynDNS ([RFC2136](https://tools.ietf.org/html/rfc2136)).

### Limitations

#### External-DNS
As of September 2019, [External-DNS](https://github.com/kubernetes-incubator/external-dns) still has some limitations:
- It doesn't support views natively
- It doesn't support CRDs of any ```kind``` other than the default one (```DNSEndpoint```).
  This is why we have a different group for the CRD corresponding to each zone/view (e.g. `internal.docs.dns-manager.cern.ch` for the `internal` view of the `doc.cern.ch` zone)
- It doesn't support as source of information an array of CRDs;
- [External-DNS](https://github.com/kubernetes-incubator/external-dns) does not watch for changes in the resources, instead it pulls them all from time to time (currently 1 minute) to update the DNS;
- Missing support for both SRV and AAAA records. This is being addressed in this [issue](https://gitlab.cern.ch/paas-tools/infrastructure/dns-manager/issues/12).
